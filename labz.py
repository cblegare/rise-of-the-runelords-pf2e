from m2r import parse_from_file
from pathlib import Path

def main():
    for filename in Path("./docs").rglob("*.md"):
        new_filename = filename.with_suffix(".rst")
        print(filename)
        print("  " + str(new_filename))

        if new_filename.stem == "README":
            new_filename = new_filename.with_stem("index")

        new_filename = Path(str(new_filename).replace(" ", "-").lower())

        new_filename.parent.mkdir(parents=True, exist_ok=True)

        content = parse_from_file(filename)
        new_filename.write_text(content)


if __name__ == "__main__":
    main()
