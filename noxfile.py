from __future__ import annotations

from pathlib import Path
import shutil
from typing import List

import nox

supported_python_versions = ["3.11"]

default_python = sorted(supported_python_versions)[-1]

repo_root = Path(__file__).parent


@nox.session(python=default_python, reuse_venv=True)
def docs(session):
    session.install("-r", "requirements.txt")
    session.run(*_python_cmd(_sphinx_build_modulecmd("build/sphinx")))


def _sphinx_build_modulecmd(build_root: str) -> List[str]:
    return [
        "sphinx",
        "-b",
        "html",
        "-d",
        f"{build_root}/doctrees",
        "-E",
        "-n",
        #"-W",
        "--keep-going",
        "-T",
        "docs",
        f"{build_root}/html",
    ]


def _python_cmd(modulecmd: List[str]) -> List[str]:
    return ["python", "-m", *modulecmd]
