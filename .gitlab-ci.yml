##############################################################################
# == CI/CD Pipeline ==========================================================
# region preamble
#####
#
# This files describe the lifecycle pipeline of our applications.
#
# Overview
#   https://docs.gitlab.com/ee/ci/README.html
#
# Keyword reference
#   https://docs.gitlab.com/ee/ci/yaml/README.html
#
# Predefined variables reference:
#   https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
#
# Environments
#   https://docs.gitlab.com/ee/ci/environments/
#
###

###
# -- Global configurations
#

stages:
  - release

cache:
  paths:
    - ~/.cache/pip
    - .nox

image: python

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      # Never run a 'branch' pipeline where this branch is under MRs.
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "$CI_DEFAULT_BRANCH"
      # Do run pipelines for schedules on the default branch
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      # Do run detached MR pipelines
      when: always
    - if: $CI_COMMIT_BRANCH
      # Do run manually triggered pipelines
      when: always
#
###

#####
# endregion
##############################################################################

##############################################################################
# -- STAGE: release ----------------------------------------------------------
# region release
#####

.release:
  stage: release

# Publish the doc as an environment
doc-review:
  extends:
    - .release
  script:
    - pip install -U nox
    - nox -rs docs
  image: python
  environment:
    name: doc-review/$CI_COMMIT_REF_SLUG
    deployment_tier: development
    on_stop: stop-doc-review
    auto_stop_in: 2 week
    url: https://$CI_PROJECT_ROOT_NAMESPACE.$CI_PAGES_DOMAIN/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html
  rules:
    # Only on branches and never on schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - when: always

# Publish the official docs on gitlab pages.
pages:
  extends:
    - .release
  script:
    - pip install -U nox
    - nox -rs docs
    - mkdir public
    - mv build/sphinx/html/* public/
  environment:
    name: production
    deployment_tier: production
    url: $CI_PAGES_URL
  artifacts:
    paths:
      - public
    expire_in: never
  rules:
    # Only run after a merge on default branch
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always

# Kill the doc environment from time to time
stop-doc-review:
  extends:
    - .release
  variables:
    GIT_STRATEGY: none
  script:
    - echo "Remove review documentation"
  environment:
    name: doc-review/$CI_COMMIT_REF_SLUG
    action: stop
  rules:
    # Run either (optionally) manually on a MR pipeline or triggered by on_stop.
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - when: manual
      allow_failure: true

#####
# endregion
##############################################################################
