
RotR Spires of Xin-Shalast (levels 16 - 20)
===========================================

Foreword
--------

This book provides an alternate for those that want to take the Adventure Path to level 20. The aim is for the PCs to reach level 20 just before the final encounter with Karzoug. To do this it:


* Assumes the party reached level 16 at the end of book 5
* Fleshes out the first part of the adventure "Seeking Xin-Shalast"
* Increases XP and adds encounters in other parts of the adventure, particularly in Xin-Shalast
* Generally applies Elite and Double Elite to creatures in the base book 6. Where those creatures were already double elite add a second creature.

Initially this book will mainly refer to the original Book 6. 

Conversion Guide for Pathfinder Second Edition (2E)
---------------------------------------------------


* This conversion guide covers the items that have changed in 2E. This includes NPCs and monsters, hazards, DCs, XP, level-based treasure, and some tactics.
* The plot, back story, color text, maps, floor plans, images and most tactics requires the original scenario.
* Creatures, hazards, items, poisons, adjustments, etc. that exist in second edition are usually linked to the relevant entry in `Archives of Nethys - second edition <https://2e.aonprd.com/>`_.
* New creatures are built using `Monster Builder <http://monster.pf2.tools/>`_ and their JSON, PDF and PNG files are in a sub directory so that you can use or update them for your own campaign as required. In some instances there are several variants available. 
* If you find an issue, error or omission, have a suggestion for improvement, or a better version of something, then comment on the discord server or create a pull request. This is a community project, and we welcome and rely on community contributions.

General GM information
----------------------


* Many encounters in this book use standard creatures, sometimes with an elite template, which are easy enough to generate without guidance. Where that is the case, then location and encounter notes will concentrate on the none-creature aspects.
* To ensure lower level 1E creatures stay relevant in 2E, these should be adjusted so they are no lower than the 4 levels below the party level. Apply elite templates as required. Note that double elite is closer to 3 additional levels rather than 2.
* DCs are not converted to 2E, except where they are contained within a creature or hazard stat block, or are particularly significant. This is left up to GM discretion.
* This is a fan created unofficial guide, and what it contains is therefore a series of suggestions, that you can take, modify, or leave as you see fit.

Index and XP distribution
-------------------------


* XP is allocated, and encounters adjusted, to achieve level 20 before the final encounter with Karzoug. 

.. list-table::
   :header-rows: 1

   * - Part
     - Title
     - Level
     - Max XP 
   * - 1
     - Seeking Xin-Shalast
     - 16
     - 1000
   * - 2
     - Whispers In The Wind
     - 17
     - 450
   * - 3
     - On the World's Roof
     - 17
     - 180
   * - 4
     - Xin-Shalast
     - 18
     - 700
   * - 5
     - Scaling Mhar Massif
     - 18
     - 120
   * - 6
     - Pinnacle of Avarice
     - 19
     - 900
   * - 7
     - The Eye of Avarice
     - 20
     - 160


TO DO
-----


* Add Seeking Xin-Shalast encounters
* Add Xin-Shalast encounters
* Adjust Book 6 original encounters
* treasure by level

Party Level and Target Allocation of Treasure
---------------------------------------------

This adventure needs to include appropriate treasure for level 16 through 19.
`Full Level to Wealth Chart <http://2e.aonprd.com/Rules.aspx?ID=581>`_

General Encounter Advice
------------------------


* Apply `Weak <http://2e.aonprd.com/Rules.aspx?ID=791>`_ or `Elite <http://2e.aonprd.com/Rules.aspx?ID=790>`_ templates as appropriate, or add/remove minions, to suit the capabilities and size of your party.  
* Usually, it is more interesting to have several low to moderate threat creatures in the encounter rather than a single severe one.

Part 1: Seeking Xin-Shalast (Party Level 16, xp 1000)
-----------------------------------------------------

  TO DO


* Vekker's Base Camp
* Thematic encounters

Part 2: Whispers In The Wind (Party Level 17, xp 450)
-----------------------------------------------------


* Apply double elite to all haunts (all DCs+4)
* Apply double elite to all creatures (+3 levels)

B1 The Tailings (xp 60, Trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Level 18 `Double Elite Horror Tree <../book-6/statblocks/HorrorTree.pdf>`_
* 
  .. image:: ../book-6/statblocks/HorrorTree.png
     :target: ../book-6/statblocks/HorrorTree.png
     :alt: Horror Tree PNG


  * `Crimson Ooze <https://2e.aonprd.com/Diseases.aspx?ID=14>`_ affects the area damaged.

B4 Sack Room
^^^^^^^^^^^^


* Treasure TODO

B5 Ore Separation (xp 10, trivial threat)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Gold Eating Dwarf <../book-6/statblocks/GoldEatingDwarf.pdf>`_
* 
  .. image:: ../book-6/statblocks/GoldEatingDwarf.png
     :target: ../book-6/statblocks/GoldEatingDwarf.png
     :alt: Gold Eating Dwarf PNG


  * This haunt is hard to resist though the effects are relatively harmless.

B6 Ore Shaft (xp 60, low)
^^^^^^^^^^^^^^^^^^^^^^^^^


* `Tipping Stairs <../book-6/statblocks/TippingStairs.pdf>`_
* 
  .. image:: ../book-6/statblocks/TippingStairs.png
     :target: ../book-6/statblocks/TippingStairs.png
     :alt: Tipping Stairs PNG

* Level 18 `Double Elite Haunted Chain <../book-6/statblocks/HauntedChain.pdf>`_
* 
  .. image:: ../book-6/statblocks/HauntedChain.png
     :target: ../book-6/statblocks/HauntedChain.png
     :alt: Haunted Chain PNG

B8 Front Porch (xp 10, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Partially Eaten Dwarf <../book-6/statblocks/PartiallyEatenDwarf.pdf>`_
* 
  .. image:: ../book-6/statblocks/PartiallyEatenDwarf.png
     :target: ../book-6/statblocks/PartiallyEatenDwarf.png
     :alt: Partially Eaten Dwarf PNG

B10 Coatroom
^^^^^^^^^^^^


* Treasure TODO

B13 Strong Room
^^^^^^^^^^^^^^^


* Treasure TODO

B14 Living Area (xp 10, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Cannibal Urgings <../book-6/statblocks/CannibalUrgings.pdf>`_
* 
  .. image:: ../book-6/statblocks/CannibalUrgings.png
     :target: ../book-6/statblocks/CannibalUrgings.png
     :alt: Cannibal Urgings PNG

B15 Larder (xp 10, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `The Hungry Dead <../book-6/statblocks/TheHungryDead.pdf>`_
* 
  .. image:: ../book-6/statblocks/TheHungryDead.png
     :target: ../book-6/statblocks/TheHungryDead.png
     :alt: The Hungry Dead PNG

Event 1: Cannibal Fury (xp 40, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Cannibal Fury <../book-6/statblocks/CannibalFury.pdf>`_
* 
  .. image:: ../book-6/statblocks/CannibalFury1.png
     :target: ../book-6/statblocks/CannibalFury1.png
     :alt: Cannibal Fury A PNG

* 
  .. image:: ../book-6/statblocks/CannibalFury2.png
     :target: ../book-6/statblocks/CannibalFury2.png
     :alt: Cannibal Fury B PNG

Event 3: Hungry Ghost (xp 100, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Double Elite Karivek Vekker <../book-6/statblocks/KarivekVekker.pdf>`_
* 
  .. image:: ../book-6/statblocks/KarivekVekker.png
     :target: ../book-6/statblocks/KarivekVekker.png
     :alt: Karivek Vekker PNG

* 2 `double elite <https://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `frost worm <https://2e.aonprd.com/Monsters.aspx?ID=677>`_

Event 4: The Siege (xp 120, severe)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Double Elite Wendigo <https://2e.aonprd.com/Monsters.aspx?ID=409>`_

Vekker Cabin Completion Bonus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* For laying the haunts to rest award a hero point and bonus 40 XP 

Part 3: On the World's Roof (Party Level 17, xp 180)
----------------------------------------------------

C. Queen of The Icemists (XP 80, Moderate, RP Encounter)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Double Elite Svevenka Ice Nymph Queen <../book-6/statblocks/svevenka.pdf>`_

  * 
    .. image:: ../book-6/statblocks/svevenkaA.png
       :target: ../book-6/statblocks/svevenkaA.png
       :alt: Svevenka A PNG

  * 
    .. image:: ../book-6/statblocks/svevenkaB.png
       :target: ../book-6/statblocks/svevenkaB.png
       :alt: Svevenka B PNG

* xp is awarded for befriending Svevenka

D. Sentinels (XP 120, Severe)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Bjormundal `Double Elite <https://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Storm Giant <https://2e.aonprd.com/Monsters.aspx?ID=225>`_
* 4x `Double Elite <https://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Cloud Giant <https://2e.aonprd.com/Monsters.aspx?ID=224>`_

Part 4: Xin-Shalast (Party Level 18, xp 700)
--------------------------------------------

Resources used throughout
^^^^^^^^^^^^^^^^^^^^^^^^^


* Wardens of Wind are (L14) `Double Elite <http://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Cloud Giants <https://2e.aonprd.com/Monsters.aspx?ID=224>`_ - These are only found in the lower city 
* Wardens of Thunder are (L16) `Double Elite <http://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Storm Giant <https://2e.aonprd.com/Monsters.aspx?ID=225>`_ and raise Chain Lightning to 8th level

Event 5: Emergence of the Spared
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

E Krak Naratha (XP 120, severe)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 12 `Krak Naratha soldiers <../book-6/statblocks/KrakNarathaSoldier.pdf>`_
* 
  .. image:: ../book-6/statblocks/KrakNarathaSoldier.png
     :target: ../book-6/statblocks/KrakNarathaSoldier.png
     :alt: Lamia Kin Kuchrecia PNG

* For Aurochs use the stats for `Woolly Rhinoceros <https://2e.aonprd.com/Monsters.aspx?ID=789>`_. This is an obstacle rather than  a threat.

F Golden Road
^^^^^^^^^^^^^

G Encampment (XP 0, zero threat, RP encounter)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Gyukak `Onidashi <https://2e.aonprd.com/Monsters.aspx?ID=746>`_

H Abominable Dome (XP 60, low)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 6 Abominable Snowman 
  .. image:: ../book-6/statblocks/AbominableSnowman.png
     :target: ../book-6/statblocks/AbominableSnowman.png
     :alt: Abominable Snowman PNG

* Add one Abominable Snowman per PC in the party over 4

I Lair of the Hidden Beast (xp 80, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 4 Vampire Skulk 
* 
  .. image:: ../book-6/statblocks/VampireSkulkA.png
     :target: ../book-6/statblocks/VampireSkulkA.png
     :alt: Vampire Skulk A PNG

* 
  .. image:: ../book-6/statblocks/VampireSkulkB.png
     :target: ../book-6/statblocks/VampireSkulkB.png
     :alt: Vampire Skulk B PNG

* `The Hidden Beast <../book-6/statblocks/TheHiddenBeast.pdf>`_

  * 
    .. image:: ../book-6/statblocks/TheHiddenBeast1.png
       :target: ../book-6/statblocks/TheHiddenBeast1.png
       :alt: The Hidden Beast 1 PNG

  * 
    .. image:: ../book-6/statblocks/TheHiddenBeast2.png
       :target: ../book-6/statblocks/TheHiddenBeast2.png
       :alt: The Hidden Beast 2 PNG

* Add one Vampire Skulk per PC in the party over 4

K Heptaric Locus (XP 80, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 
  Gamigin `double elite <http://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Ice Devil <https://2e.aonprd.com/Monsters.aspx?ID=113>`_


  * The Cone of Colds are raised to 8th level
  * Add Chilling Darkness, Finger of Death and Regenerate at 7th level
  * Add Teleport at 6th Level
  * Add Darkness(x2) at 4th level

* 
  2 Scarlet Walker 
  .. image:: ../book-6/statblocks/ScarletWalker.png
     :target: ../book-6/statblocks/ScarletWalker.png
     :alt: Scarlet Walker PNG


* Scarlet Walker Spells 
  .. image:: ../book-6/statblocks/ScarletWalkerSpells.png
     :target: ../book-6/statblocks/ScarletWalkerSpells.png
     :alt: Scarlet Walker Spells PNG

N Hidden Path (XP 30, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Mountain Roper 
  .. image:: ../book-6/statblocks/MountainRoper.png
     :target: ../book-6/statblocks/MountainRoper.png
     :alt: Mountain Roper PNG

Q Ghlorofaex's Lair (xp 60, low)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Ghlorofaex `Elite Ancient Blue Dragon <https://2e.aonprd.com/Monsters.aspx?ID=132>`_

  * Choose spellcaster or frenzy/momentum according to taste

Xin-Shalast completion bonus (xp 80)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* For navigating through Xin-Shalast and finding their way up award bonus 80XP and a hero point

Part 5: Scaling Mhar Massif (Party Level 18, xp 120)
----------------------------------------------------

Invaders from Leng (xp 120, severe)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 6 `Double Elite <http://2e.aonprd.com/Rules.aspx?ID=790>`_\ : `Leng Spiders <https://2e.aonprd.com/Monsters.aspx?ID=713>`_

Part 6: Pinnacle of Avarice (Party Level 19, xp 900)
----------------------------------------------------


* Be wary of running too many encounters together, or of casting too many area affect spells from groups of identical creatures.
* Use the politics to your advantage here. While all the leaders (Viorian, Khalib, Ceoptra) are loyal to Karzoug, they each want and expect to be next in line to be the future runelord of greed.

X1 Entrance Ramp (xp 60, low)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* There is one warden of thunder for each party member

X2 Kharzoug (xp 10 x 6, trivial, RP opportunities)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* The images provide good opportunities for roleplay and for each side to weigh up the capabilities of their opponents. For example they get to understand the power of Karzoug's spells without major risk and possibly his counterspell / reflect spell ability. Karzoug tries to weigh up who is the greatest threat.
* each image destroyed is worth 10 xp
* Kharzoug generally only casts through these images if he can help a combat, using spells such as Slow, Wall of Force, Resilient Sphere, Black Tentacles, Earthbind, Aqueous Orb, Telekinetic Maneuver, Grease, Glitterdust (from the staff).
* 
  Karzoug will however try and counter and potentially reflect any spell he sees being cast.

* 
  Destroying the images weakens the link between the eye of avarice and xin-shalast and delays Karzoug's return.

X3 Throne Rooom (xp 90, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Viorian Dekanti, Champion of Greed <../book-6/statblocks/ViorianDekanti.pdf>`_

  * 
    .. image:: ../book-6/statblocks/ViorianDekanti1.png
       :target: ../book-6/statblocks/ViorianDekanti1.png
       :alt: Viorian Dekanti 1 PNG

  * 
    .. image:: ../book-6/statblocks/ViorianDekanti2.png
       :target: ../book-6/statblocks/ViorianDekanti2.png
       :alt: Viorian Dekanti 2 PNG

* 2 Wardens of Thunder
* add a Warden of Thunder for each PC in the party over 4
* `Chellan, Sword of Greed <../book-6/statblocks/ChellanSwordofGreed.pdf>`_
* 
  .. image:: ../book-6/statblocks/ChellanSwordofGreed.png
     :target: ../book-6/statblocks/ChellanSwordofGreed.png
     :alt: Chellan, Sword of Greed PNG

X4 Lair (xp 30, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^


* Warden of Runes `Rune Giant <https://2e.aonprd.com/Monsters.aspx?ID=226>`_

X5 Kin Quarters
^^^^^^^^^^^^^^^


* Treasure TODO

X6 Cells (xp 60, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^


* Add Elite to 4 `Lamia Priestess <../book-6/statblocks/lamiaPriestess.pdf>`_

  * 
    .. image:: ../book-6/statblocks/lamiaPriestessA.png
       :target: ../book-6/statblocks/lamiaPriestessA.png
       :alt: Lamia Priestess A PNG

  * 
    .. image:: ../book-6/statblocks/lamiaPriestessB.png
       :target: ../book-6/statblocks/lamiaPriestessB.png
       :alt: Lamia Priestess B PNG

X7 Den (xp 30, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^


* Add Elite to 2 `Lamia Kin Hungerer <../book-6/statblocks/hungerer.pdf>`_

  * 
    .. image:: ../book-6/statblocks/hungerer.png
       :target: ../book-6/statblocks/hungerer.png
       :alt: Hungerer PNG

X8 Prison (xp 30, trivial threat)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Purrodaemon <https://2e.aonprd.com/Monsters.aspx?ID=596>`_

X9 Chambers
^^^^^^^^^^^


* Treasure TODO

X10 Door (xp 40, trivial threat, RP potential)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Ayruzi `Double Elite Planetar <https://2e.aonprd.com/Monsters.aspx?ID=544>`_  

X11 Doors (xp 30)
^^^^^^^^^^^^^^^^^


* `Aklo Doors <../book-6/statblocks/AkloDoors.pdf>`_

  * 
    .. image:: ../book-6/statblocks/AkloDoors.png
       :target: ../book-6/statblocks/AkloDoors.png
       :alt: Aklo Doors PNG

X12 The Device (xp 120 + 40 + 40, severe+)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Add double elite to 8 `Pinnacle of Leng <../book-6/statblocks/PinnacleOfLeng.pdf>`_

  * 
    .. image:: ../book-6/statblocks/PinnacleOfLeng.png
       :target: ../book-6/statblocks/PinnacleOfLeng.png
       :alt: Pinnacle of Leng PNG

* 
  Add Elite to `The Thing From Beyond Time <../book-6/statblocks/TheThingFromBeyondTime.pdf>`_


  * 
    .. image:: ../book-6/statblocks/TheThingFromBeyondTime.png
       :target: ../book-6/statblocks/TheThingFromBeyondTime.png
       :alt: The Thing From Beyond Time PNG

  * Remember that the Occluding Field stops Angled Entry from working and causes it extreme discomfort and pain.

* 
  For stopping the completion of the Leng Device award bonus 40 XP and a hero point.

X13 Cells (xp 100, moderate)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* 4 Wardens of Thunder
* Warden of Runes `Rune Giant <https://2e.aonprd.com/Monsters.aspx?ID=226>`_
* image of Karzoug
* add another Warden of Thunder for each member in the party over 4
* Khalib is likely to join this combat on round 4 or 5 after prepping

X15 Quarters (xp 40, trivial if encountered here)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Add Elite to `First Apprentice Khalib <../book-6/statblocks/Khalib.pdf>`_

  * 
    .. image:: ../book-6/statblocks/Khalib1.png
       :target: ../book-6/statblocks/Khalib1.png
       :alt: Khalib PNG 1

  * 
    .. image:: ../book-6/statblocks/Khalib2.png
       :target: ../book-6/statblocks/Khalib2.png
       :alt: Khalib PNG 2

  * Khalib is likely to join other (less than severe) combats in the Pinnacle rather than be found here
  * If Khalib is encountered alone here, consider adding Wardens to the fight.
  * Khalib is essentialy the practice run for Karzoug - both for the players and for the GM

Tactics:
~~~~~~~~


* Khalib wont go to the assistance of either Viorian or Ceoptra (or they to his).
* Before combat

  * Khalib casts darkvision, false life, longstrider, see invisibility, tongues, and mind blank on himself every day.
  * Once an alarm is raised, he also casts stoneskin, true seeing, Fiery Body (which can fly) on himself before entering battle.

* During Combat

  * At the start of combat Khalib casts quickened Slow and Flesh to Stone. On the second round he casts Duplicate Foe (not living so not affected by the occluding field). 
  * Following up with free action sustained Duplicate Foe, and whatever spell he thinks appropriate.
  * He uses (clever) counterspell with reflect spell to target enemy casters.
  * The first attack against him or non-counterspellable effect of each round uses his Foresight reaction.
  * If he has any actions left he casts shield.

X16 Reliquary (xp 30, trivial)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* `Shemhazian Demon <https://2e.aonprd.com/Monsters.aspx?ID=102>`_
* The Wall of Force counts as a 9th level spell (AC 10, hardness 30, hit points 120) and a 18th level hazard. It repairs itself at a rate of 5 hit points per round. The rebound energy deals 6d12 force damage, DC 40 basic Reflex. Grant the party XP as for an 18th level hazard when overcoming this hazard.

X17 Chamber of Focus (xp 105, severe)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Most High Ceoptra TO DO
* 3 Wardens of Thunder

Part 7: The Eye of Avarice (Party Level 20, xp 160)
---------------------------------------------------


* 
  This is the epic final battle. Review carefully and balance to your party. Its final makeup should not be over 40XP per PC. Though note with intelligent play the Wardens can change sides.


  * For additional PCs make the dragon Double Elite. 

* 
  The layout of this room needs some consideration and potential adjustment: 


  * Consider adjusting the throne to be on a further raised dais and lower the runewell by 20 ft thereby having the entire room on a slope where Karzoug can see down the centre and has line of sight for his spells without getting up.
  * Consider making all the walkways 15ft wide as the dragon and the 3 gaints are all huge
  * The huge dragon has a 60 foot wingspan. If it is to fly consider having all the gold columns stop level with the adjacent walkways, and 40-60ft of free space above them.

The Final Battle (xp 160, Extreme)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* Add Elite to `Karzoug the Claimer <../book-6/statblocks/Karzoug.pdf>`_

  * 
    .. image:: ../book-6/statblocks/Karzoug1.png
       :target: ../book-6/statblocks/Karzoug1.png
       :alt: Karzoug 1 PNG

  * 
    .. image:: ../book-6/statblocks/Karzoug2.png
       :target: ../book-6/statblocks/Karzoug2.png
       :alt: Karzoug 2 PNG

* `Fulmina - Ancient Blue Dragon <https://2e.aonprd.com/Monsters.aspx?ID=132>`_ - consider using the Frenzy/Momentum version as there are quite enough spellcasters here already
* Warden of Runes `Teth - Double Elite Rune Giant <https://2e.aonprd.com/Monsters.aspx?ID=226>`_
* 2 Wardens of Thunder (Berg and Gil)

Tactics:
~~~~~~~~


* The general tactics are that the Dragon is a distraction, the Rune Giant is a bodyguard to allow Karzoug to keep casting, and the Wardens are ranged support.
* Before combat

  * Karzoug casts darkvision, longstrider, see invisibility, tongues, and mind blank on himself every day. 

* ROUND 1: 

  * Karzoug casts time stop and inside that casts Foresight, Stoneskin and Fiery Body (which can fly).
  * The blue dragon begins the fight next to Karzoug’s throne, and flies out toward the PCs to breathe lightning on them, and to position herself to be a wall of dragon. 
  * The rune giant begins between the runewell and the throne. He casts true seeing and then air walks to stay between the PCs and Karzoug as a wall of giant. 
  * A warden of thunder stands atop each of the balconies at area Y2. Each of them casts chain lightning. 

* ROUND 2: 

  * Karzoug casts Meteor Storm and quickened horrid wilting      
  * The blue dragon hinders using physical attacks and breathes lightning if she can.
  * The rune giant defends as Karzoug's bodyguard.
  * The wardens of thunder cast chain lightning again

* ROUND 3: 

  * Karzoug casts true strike and spell-combination double disintegrate
  * The rune giant continues to protect Karzoug. 
  * The wardens of thunder cast chain lightning again
  * The blue dragon hinders using physical attacks and breathes lightning if she can.

* 
  REMAINDER OF COMBAT:


  * Karzoug continues casting offensive spells like flesh to stone. He sets his glaive dancing. 
  * The rune giant continues to protect Karzoug. 
  * The blue dragon continues to distract.
  * The wardens of thunder enter melee.

* 
  Morale


  * If the rune giant is killed the Wardens switch sides, and the PCs should be suspecting this might happen by now. 
  * Everyone else fights to the death - unless Karzoug is killed.

  ## - End of RotR Spires of Xin-Shalast 2E Conversion Guide -
