##########################
Backgrounds and references
##########################

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    authors-notes
    conversion-overview
    rotr-campaign-backgrounds
    subsystems
